import os
import re
import sys
import glob
import numpy
import argparse
import tifffile

import warnings
warnings.filterwarnings("ignore")

__author__ = "christoph.sommer@ist.ac.at"
__licence__ = "GPLv3"
__version__ = "0.1.0"

description = \
"""
Convert multi-file OME-Tiff files produced at Open-SPIM microscope to ImageJ hyper-stacks
"""
        
pat = r'spim_dataset_CH_(?P<channel>.+)_TL(?P<frame>.+)_Angle0.ome.tiff'

def get_args():
    parser = argparse.ArgumentParser(
        description=description)
    
    parser.add_argument('input_folders', type=str, nargs='+', help="List of input folders containing OME-Tiff files")

    return parser.parse_args()

def convert(in_dir):
    assert os.path.exists(in_dir), " - Folder does not exist '{}'".format(in_dir)

    ome_fns = glob.glob(os.path.join(in_dir, "*.ome.tiff"))

    assert len(ome_fns) > 0, " - No ome.tiff files found in {}'".format(in_dir)

    # read first for spatial dims from frist file
    tmp = tifffile.imread(ome_fns[0], is_ome=False)

    dtype = tmp.dtype
    z, y, x = tmp.shape

    channel_set = set()
    frame_set = set()

    fn_lkp = {}
    for fn in ome_fns:
        fn_base = os.path.basename(fn)
        match = re.search(pat, fn_base)
        groupdict = match.groupdict()
        channel = groupdict["channel"]
        frame = groupdict["frame"]
        
        fn_lkp[(channel, frame)] = fn

        channel_set.add(channel)
        frame_set.add(frame)
    
    out_hyper = numpy.zeros((len(frame_set), z, len(channel_set), y, x), dtype=dtype)

    print(" - Output shape TZCYX", out_hyper.shape)

    for c, channel in enumerate(sorted(channel_set)):
        for f, frame in enumerate(sorted(frame_set, key=lambda t: int(t))):
            print(" - Reading", fn_lkp[(channel, frame)], c, f)
            tmp = tifffile.imread(fn_lkp[(channel, frame)], is_ome=False)
            out_hyper[f, :, c, :, :] = tmp

    out_fn = "Resaved.tif"
    print(" - Resaving to", out_fn)
    tifffile.imsave(os.path.join(in_dir, out_fn), out_hyper, imagej=True)

def main():
    args = get_args()

    for in_dir in args.input_folders:
        print("Converting ome-tiffs in '{}'".format(in_dir))
        convert(in_dir)


    







