# -*- coding: utf-8 -*-
 
"""setup.py: setuptools control."""
 
import re
from setuptools import setup
 
 
version = re.search(
    '^__version__\s*=\s*"(.*)"',
    open('convert_ometiff/convert_ometiff.py').read(),
    re.M
    ).group(1)
 
 
with open("README.md", "rb") as f:
    long_descr = f.read().decode("utf-8")

description = \
"""
Convert multi-file OME-Tiff files produced at Open-SPIM microscope to ImageJ hyper-stacks
"""
 
setup(
    name = "convert_ometiff",
    packages = ["convert_ometiff"],
    entry_points = {
        "console_scripts": ['convert_ometiff = convert_ometiff.convert_ometiff:main']
        },
    version = version,
    description = description,
    long_description = long_descr,
    author = "Christoph Sommer",
    author_email = "christoph.sommer@gmail.com",
    )