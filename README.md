# convert_ometiff
Convert multi-file OME-Tiff files produced at Open-SPIM microscope to ImageJ hyper-stacks. Note, the pixel-size information is lost

## Installation
---

1. Recommended: download and install Python 3.6 using the Anaconda python distribution 
   from https://www.anaconda.com/download/. Anaconda already comes with 
   most of the required packages.
2. Download convert_ometiff and/or extract the convert_ometiff package to path/to/dir
3. Open (Anaconda) prompt and type 
```
cd path/to/dir
```
4. Install additional requirements
```
pip install -r requirements.txt
```
(Windows) If installing tifffile fails, download the tifffile .whl package from https://www.lfd.uci.edu/~gohlke/pythonlibs/ and install it with
```
pip install <tifffile-..-.whl>
```
5. Install the convert_ometiff package
```
python setup.py install
```
6. Quick test
```
convert_ometiff --help
```
If all went fine, the command line help is displayed.

## Usage

Open (Anaconda prompt)
```
convert_ometiff <dir/containint/ometiffs> [<dir/containing/ometiffs>]
```

## Planned updates
1. Include meta-data such as pixel-sizes
2. More flexible pattern matching